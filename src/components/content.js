import { Component } from "react";
import Input from "./Input";
import Output from "./Output";
import 'bootstrap/dist/css/bootstrap.min.css';

class Content extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-6">
                        <Input/>
                    </div>
                    <div className="col-sm-6">
                        <Output/>
                    </div>
                </div> 
            </div>
        )
    }
}

export default Content