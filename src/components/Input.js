import { Component } from "react";
import ImageLuckyDice from "../../src/assets/images/people-playing-casino.jpg"

class Input extends Component {
    render() {
        return (
            <div className="text-center mt-5">
                <h2>Devcamp lucky dice</h2>
                <div>
                    <img className="mt-4" src={ImageLuckyDice} alt="ImageLuckyDice" width="500px"></img>
                </div>
            </div>
        )
    }
}

export default Input