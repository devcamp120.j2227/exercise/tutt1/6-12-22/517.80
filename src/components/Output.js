import { Component } from "react";
import ImageDice from "../assets/images/dice.png";
import Dice1 from "../assets/images/1.png";
import Dice2 from "../assets/images/2.png";
import Dice3 from "../assets/images/3.png";
import Dice4 from "../assets/images/4.png";
import Dice5 from "../assets/images/5.png";
import Dice6 from "../assets/images/6.png";

class Output extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentImage: ImageDice
        }
    }
    rollClick = () => {
        let Dice = Math.floor(Math.random() * 6 + 1);
        if(Dice === 1) {
            this.setState({
                currentImage: Dice1
            })
        }
        if(Dice === 2) {
            this.setState({
                currentImage: Dice2
            })
        }
        if(Dice === 3) {
            this.setState({
                currentImage: Dice3
            })
        }
        if(Dice === 4) {
            this.setState({
                currentImage: Dice4
            })
        }
        if(Dice === 5) {
            this.setState({
                currentImage: Dice5
            })
        }
        if(Dice === 6) {
            this.setState({
                currentImage: Dice6
            })
        }
    }
    render() {
        return (
            <div className="text-center mt-5">
                <h4>Quay xúc xắc</h4>
                <div className="mt-5">
                    <img src={this.state.currentImage} alt="Dice" width="300px"></img>
                </div>
                <div className="mt-5">
                    <button className="btn btn-success" onClick={() => this.rollClick()}>Ném</button>
                </div>
            </div>
        )
    }
}

export default Output